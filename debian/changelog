getmail (5.13-1) unstable; urgency=medium

  * New upstream version (Fix SNI change with Python < 2.7 for
    backport compatibility)

 -- Osamu Aoki <osamu@debian.org>  Sun, 24 Feb 2019 22:49:27 +0900

getmail (5.11-2) unstable; urgency=medium

  * Drop --store-password-in-gnome-keyring support since
    python-gnomekeyring is not available any more on Debian.
    Closes: #912963

 -- Osamu Aoki <osamu@debian.org>  Wed, 20 Feb 2019 23:53:40 +0900

getmail (5.11-1) unstable; urgency=medium

  * New upstream version
   - sends SNI if available in underlying TLS library

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 14 Feb 2019 17:31:35 -0500

getmail (5.9-1) unstable; urgency=medium

  * New upstream version (docs update)
  * Standards-Version: bump to 4.3.0 (no changes needed)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 08 Feb 2019 10:24:57 -0500

getmail (5.8-1) unstable; urgency=medium

  * New upstream version

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 13 Nov 2018 23:23:15 -0500

getmail (5.7-3) unstable; urgency=medium

  * use pybuild and python-all instead of python2 (Closes: #912435)
  * revert earlier buildd debugging

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 31 Oct 2018 17:38:18 -0400

getmail (5.7-2) unstable; urgency=medium

  * diagnose before failing on buildd

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 31 Oct 2018 14:48:00 -0400

getmail (5.7-1) unstable; urgency=medium

  * New upstream release
  * d/changelog: strip trailing whitespace
  * standards-version: bump to 4.2.1 (no changes needed)
  * wrap-and-sort -ast
  * canonicalize VCS-Git
  * convert fully to dh_python2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 30 Oct 2018 13:35:15 -0400

getmail (5.6-1) unstable; urgency=medium

  * New upstream release.
    + fix references to version 4 in README.  Thanks: Daniel Kahn Gillmor.
    + add Gmail-specific XOAUTH2 login support for IMAP.  Thanks: Stefan
      Krah.
  * Add recommend for --store-password-in-gnome-keyring.  Closes: #892978

 -- Osamu Aoki <osamu@debian.org>  Sun, 01 Jul 2018 12:10:43 +0900

getmail (5.5-3) unstable; urgency=medium

  * d/control: repoint Vcs-*: to use salsa.debian.org
  * Use python2 in shebang lines (Closes: #890282)
  * d/control: set Rules-Requires-Root: no
  * d/rules: drop trailing whitespace
  * Standards-Version: bump to 4.1.3 (no changes needed)
  * move to debhelper 11
  * fix all shipped shebang lines by tweaking setup.cfg
  * d/control: depend on python2.7 explicitly.
  * added myself to uploaders

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 21 Feb 2018 10:04:07 -0800

getmail (5.5-2) unstable; urgency=medium

  * Fix regression caused by transition package. Closes: #884807
  * Package clean-up by Daniel Kahn Gillmor.  Thanks.
  * Proper versioned Breaks.

 -- Osamu Aoki <osamu@debian.org>  Thu, 21 Dec 2017 01:32:19 +0900

getmail (5.5-1) unstable; urgency=medium

  * New upstream release.
    + Add record_mailbox configuration parameter.
      Closes: #833190, #884757
  * Add dummy transition package getmail4 for smooth migration.

 -- Osamu Aoki <osamu@debian.org>  Wed, 20 Dec 2017 00:31:00 +0900

getmail (5.4-2) unstable; urgency=medium

  * Pass parameters to "getmails -p".  Closes: #884691
  * Refine wordings in the manpage and README.Debian.

 -- Osamu Aoki <osamu@debian.org>  Mon, 18 Dec 2017 00:18:31 +0900

getmail (5.4-1) unstable; urgency=medium

  * New upstream release.  Closes: #877909
  * Change the package name from getmail4 to getmail.
  * Update policy to 4.1.2 and debhelper to compat=10
  * Skip editor generated files such as foo~ for the configuration
    file.  Closes: #863856
  * Add an optional -p option to getmails.  Closes: #873040

 -- Osamu Aoki <osamu@debian.org>  Sun, 17 Dec 2017 21:45:09 +0900

getmail4 (4.53.0-2) unstable; urgency=medium

  * This patch fixes a single error in the getmail_fetch command
    introduced in 4.53.0 and fixed in 4.54.0.  This also contains the
    patch for the upstream version used in the source to be bump to
    4.54.0 This should make users not to complain about the buggy
    version in stable.  Closes: #877916

 -- Osamu Aoki <osamu@debian.org>  Sat, 07 Oct 2017 16:38:27 +0900

getmail4 (4.53.0-1) unstable; urgency=medium

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Tue, 24 Jan 2017 00:15:29 +0900

getmail4 (4.52.0-1) unstable; urgency=medium

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Wed, 07 Dec 2016 01:58:43 +0900

getmail4 (4.50.0-1) unstable; urgency=medium

  * New upstream release.  Closes: #834254
    + Enable the read-only IMAP folder with version 4.49.0
    + Fix getmail error on IMAP folders containing i18n characters
      with version 4.50.0

 -- Osamu Aoki <osamu@debian.org>  Tue, 23 Aug 2016 22:13:23 +0900

getmail4 (4.48.0-1) unstable; urgency=medium

  * New upstream release.
    + 4.47.0: Fix IMAP IDLE mode with delete_after. Closes: #778893
    + 4.48.0: set poplib._MAXLINE to 1 MB. Closes: #782614

 -- Osamu Aoki <osamu@debian.org>  Mon, 01 Jun 2015 22:46:33 +0900

getmail4 (4.46.0-1) unstable; urgency=medium

  * New upstream release. Closes: #745484
    + hostname-vs-certificate matching of SSL certificate
      with proper checking of Python version.

 -- Osamu Aoki <osamu@debian.org>  Tue, 22 Apr 2014 23:47:34 +0900

getmail4 (4.44.0-1) unstable; urgency=medium

  * New upstream release.
    + Add extended SSL options for IMAP retrievers.

 -- Osamu Aoki <osamu@debian.org>  Sun, 30 Mar 2014 10:44:21 +0900

getmail4 (4.43.0-1) unstable; urgency=low

  * New upstream release.
    + Fix non-ascii characters handling in message header fields.
    + Add IMAP IDLE support. Closes: #301690

 -- Osamu Aoki <osamu@debian.org>  Sat, 31 Aug 2013 08:21:36 +0900

getmail4 (4.41.0-2) unstable; urgency=low

  * Remove patches not needed any more to make sure script lines are not
    shifted.

 -- Osamu Aoki <osamu@debian.org>  Fri, 02 Aug 2013 22:15:28 +0900

getmail4 (4.41.0-1) unstable; urgency=low

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Mon, 10 Jun 2013 00:07:25 +0900

getmail4 (4.40.3-3) unstable; urgency=low

  * Remove Fredrik Steen from uploader.
  * Revert Microsoft Exchange Server 2003 patch.  Closes: #709170

 -- Osamu Aoki <osamu@debian.org>  Sat, 01 Jun 2013 00:43:40 +0900

getmail4 (4.40.3-2) unstable; urgency=low

  * Add missing import by Ana Beatriz Guerrero Lopez. Closes: #708797

 -- Osamu Aoki <osamu@debian.org>  Tue, 21 May 2013 00:49:25 +0900

getmail4 (4.40.3-1) unstable; urgency=low

  * New upstream releases merged for jessie.
    - non-ASCII chars in IMAP folder name. Closes: #513116, #701717
  * Enhance compatibility with Microsoft Exchange Server 2003
    with patch by W. Martin Borgert.  Closes: #701844

 -- Osamu Aoki <osamu@debian.org>  Fri, 17 May 2013 23:58:30 +0900

getmail4 (4.32.0-2) unstable; urgency=low

  * Prevent mail data corruption by the mboxo format by applying
    upstream 4.35 patch to use the mboxrd format. Closes: #633799

 -- Osamu Aoki <osamu@debian.org>  Fri, 16 Nov 2012 20:31:42 +0900

getmail4 (4.32.0-1) unstable; urgency=low

  * New upstream release.
    - Prevent some nuisance stack traces if getmail cannot connect to the
      POP/ IMAP server correctly.
    - Restore use_peek IMAP retriever parameter which accidentally got
      removed in 4.30.
    - Improved backwards compatibility with pre-v.4.22.0 oldmail files,
      so IMAP mail is not re-retrieved if you upgrade from a 4.22 or
      earlier.  This is for Debian system upgrading from squeeze (4.20.0).

 -- Osamu Aoki <osamu@debian.org>  Thu, 12 Jul 2012 00:37:18 +0900

getmail4 (4.30.2-1) unstable; urgency=low

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Thu, 28 Jun 2012 21:24:08 +0900

getmail4 (4.30.1-1) unstable; urgency=low

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Fri, 22 Jun 2012 21:40:57 +0900

getmail4 (4.30.0-1) unstable; urgency=low

  * New upstream release. Closes: #678411

 -- Osamu Aoki <osamu@debian.org>  Fri, 22 Jun 2012 00:12:39 +0900

getmail4 (4.29.0-2) unstable; urgency=low

  * Upstream provided bug fix for BrokenUIDLPOP3Retriever. Closes: #678330

 -- Osamu Aoki <osamu@debian.org>  Thu, 21 Jun 2012 22:45:16 +0900

getmail4 (4.29.0-1) unstable; urgency=low

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Wed, 20 Jun 2012 20:52:41 +0900

getmail4 (4.27.0-1) unstable; urgency=low

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Fri, 25 May 2012 07:32:20 +0900

getmail4 (4.26.0-1) unstable; urgency=low

  * New upstream release.
   + Use BODY.PEEK in IMAP retrieval. Closes: #668602
  * Avoid $UID in getmails for BASH compatibility. Closes: #665756
  * Swap maintainer and uploader to reflect recent situation.
  * Updated policy version and made DEP-5 compliant.

 -- Osamu Aoki <osamu@debian.org>  Sun, 15 Apr 2012 14:04:42 +0900

getmail4 (4.24.0-1) unstable; urgency=low

  * New upstream release.

 -- Osamu Aoki <osamu@debian.org>  Wed, 14 Dec 2011 21:38:49 +0900

getmail4 (4.23.0-2) unstable; urgency=low

  * Used -U option for pgrep.

 -- Osamu Aoki <osamu@debian.org>  Tue, 22 Nov 2011 22:02:27 +0900

getmail4 (4.23.0-1) unstable; urgency=low

  * New upstream release to fix race in POP3 mailbox.  Thanks to
    Roland Koebler. Closes: #648242

 -- Osamu Aoki <osamu@debian.org>  Mon, 21 Nov 2011 21:51:52 +0900

getmail4 (4.22.2-1) unstable; urgency=low

  * New upstream release to fix IMAP regression in 4.21.0.

 -- Osamu Aoki <osamu@debian.org>  Sun, 13 Nov 2011 23:14:06 +0900

getmail4 (4.22.1-1) unstable; urgency=low

  * New upstream release.
    + Improved retriever support for multiple IMAP mail folders.
    + Automatically open IMAP folders read-only if neither the delete nor
      delete_after options are in use; necessary to access chatlogs via Gmail.
    + Avoid sorting msgids on each retrieval, reducing overhead when dealing
      with folders containing thousands of messages.
  * Added a Debian specific wrapper script, getmails.

 -- Osamu Aoki <osamu@debian.org>  Sun, 13 Nov 2011 00:01:08 +0900

getmail4 (4.20.4-1) unstable; urgency=low

  * New upstream release.
    + Accepted "getmail_retrievers-IMAP-error.patch".
    + switch to using Parser instead of HeaderParser
      to correct illegal formatting bogosities in the
      body parts of incoming messages. Closes: #633735
  * Moved homepage-entity in control file to the source side.

 -- Osamu Aoki <osamu@debian.org>  Fri, 22 Jul 2011 23:31:54 +0900

getmail4 (4.20.3-2) unstable; urgency=low

  * Removed Debian patches for Python path since the new policy
    compliant packaging style does not require them. Closes: #633641

 -- Osamu Aoki <osamu@debian.org>  Tue, 12 Jul 2011 21:27:55 +0900

getmail4 (4.20.3-1) unstable; urgency=low

  * New upstream release
  * Bumped to "Standards-Version: 3.9.2".
  * Python policy updates such as use of "dh $@ --with python2".
  * Improved IMAP error messages. Closes: #606972
  * DEP-3 compliant patch files.
  * DEP-5 debian/copyright file.

 -- Osamu Aoki <osamu@debian.org>  Mon, 11 Jul 2011 23:50:52 +0900

getmail4 (4.20.2-1) unstable; urgency=low

  * New upstream release.
  * Bumped to "Standards-Version: 3.9.1".

 -- Fredrik Steen <stone@debian.org>  Mon, 11 Apr 2011 13:26:40 +0200

getmail4 (4.20.0-1) unstable; urgency=low

  * Added Osamu Aoki as Uploaders.
  * Updated debian/watch file to run uupdate.
  * Plcaced this package in collab-maint at git.debian.org.
  * Fixed broken internal links. Closes: #463897
  * New upstream release. Closes: #587651
  * Included upstream chengelog in the deb package.

 -- Osamu Aoki <osamu@debian.org>  Sat, 03 Jul 2010 09:24:06 +0900

getmail4 (4.17.0-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release
    - fixing 500 message issues with some IMAP servers that that return
      oddball responses to SELECT.
  * Changed to "dh $@" style and removed unused debian/pycompat.
  * Enforced Python 2.4 or newer. Closes: #294939
  * Moved systematic changes to sed -i in the override_dh_install step.
  * Moved to 3.0 (quilt) source format and cleaned up patch formats.

 -- Osamu Aoki <osamu@debian.org>  Wed, 05 May 2010 02:00:43 +0900

getmail4 (4.16.0-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release
    - fixing default port for MultidropIMAPSSLRetriever.
    - continuing getmail for IMAP exceptions. Closes: #481380, 443856
  * Fixed watch file not to fetch beta version from 5 series.
  * Removed useless debian/getmail4.dirs.
  * Bumped to "Standards-Version: 3.8.4".
  * Added debian/source/format.
  * Fixed lintian build-depends-on-python-dev-with-no-arch-any.
  * Fixed lintian possible-documentation-but-no-doc-base-registration

 -- Osamu Aoki <osamu@debian.org>  Thu, 29 Apr 2010 00:07:20 +0900

getmail4 (4.14.0-2) unstable; urgency=low

  * Updated to versioned build depends on debhelper 7.
  * Added misc-depends to Depends.
  * Now lintian clean.

 -- Fredrik Steen <stone@debian.org>  Tue, 22 Dec 2009 18:11:37 +0100

getmail4 (4.14.0-1.1) unstable; urgency=low

  * Non-maintainer upload with approval by the mainatainer.
  * Updated __init__.py-remove-hash-bang.patch.
  * Set debhelper compatibility level to 7
  * Updated to standards version 3.8.3

 -- Osamu Aoki <osamu@debian.org>  Sun, 29 Nov 2009 00:55:08 +0900

getmail4 (4.8.4-1) unstable; urgency=low

  * Updated to standards version 3.8.0.0
  * Added debian/watch
  * New Upstream Version

 -- Fredrik Steen <stone@debian.org>  Wed, 22 Oct 2008 21:29:53 +0200

getmail4 (4.7.8-1) unstable; urgency=low

  * New upstream release (Closes:#462020)

 -- Fredrik Steen <stone@debian.org>  Fri, 15 Feb 2008 16:10:41 +0100

getmail4 (4.7.7-1) unstable; urgency=low

  * New upstream release Closes:#453691

 -- Fredrik Steen <stone@debian.org>  Sun, 02 Dec 2007 21:09:11 +0100

getmail4 (4.7.6-2) unstable; urgency=low

  * Updated copyright file to point to GPL-2, thanks Sven Joachim
    Closes:#439834

 -- Fredrik Steen <stone@debian.org>  Sun, 26 Aug 2007 09:44:42 +0000

getmail4 (4.7.6-1) unstable; urgency=low

  * New upstream release
  * Upstream fix for external MDA stderr handling. Closes:#400304

 -- Fredrik Steen <stone@debian.org>  Sat, 25 Aug 2007 19:25:51 +0000

getmail4 (4.7.1-1) unstable; urgency=low

  * New upstream release

 -- Fredrik Steen <stone@debian.org>  Tue, 20 Feb 2007 14:36:22 +0000

getmail4 (4.6.5-1) unstable; urgency=low

  * New upstream release

 -- Fredrik Steen <stone@debian.org>  Mon, 23 Oct 2006 11:17:40 +0200

getmail4 (4.6.2-3) unstable; urgency=high

  * Added correct search path for getmail_mbox, getmail_fetch and
    getmail_maildir. (Closes:#375675)

 -- Fredrik Steen <stone@debian.org>  Tue, 11 Jul 2006 09:33:34 +0200

getmail4 (4.6.2-2) unstable; urgency=high

  * Patch to supress FutureWarnings from Optparse (#290637)
  * Upgraded package to comply with new python policy
  * Updated sys.path to include getmailcore (Closes:#375675)

 -- Fredrik Steen <stone@debian.org>  Thu, 15 Jun 2006 13:41:24 +0200

getmail4 (4.6.2-1) unstable; urgency=low

  * New upstream release.
  * Updated to new python policy.

 -- Fredrik Steen <stone@debian.org>  Wed, 14 Jun 2006 12:46:39 +0200

getmail4 (4.5.4-1) unstable; urgency=low

  * New upstream release Closes:#351788
  * keyfile and certfile parameters to SSL
    retrievers are now expanded Closes:#351291

 -- Fredrik Steen <stone@debian.org>  Fri, 24 Mar 2006 12:29:43 +0100

getmail4 (4.3.5-1) unstable; urgency=low

  * New upstream release

 -- Fredrik Steen <stone@debian.org>  Tue,  5 Apr 2005 14:32:08 +0200

getmail4 (4.3.3-1) unstable; urgency=low

  * Changed URL to homepage (thanks Stephan Gromer)
  * Changed URL to homepage is README.Debian Closes:#298642

 -- Fredrik Steen <stone@debian.org>  Mon, 28 Feb 2005 08:17:45 +0100

getmail4 (4.3.2-1) unstable; urgency=low

  * New upstream release

 -- Fredrik Steen <stone@debian.org>  Fri, 18 Feb 2005 08:28:01 +0100

getmail4 (4.2.5-1) unstable; urgency=low

  * New upstream release

 -- Fredrik Steen <stone@debian.org>  Thu,  9 Dec 2004 14:47:44 +0100

getmail4 (4.2.4-1) unstable; urgency=low

  * New upstream release

 -- Fredrik Steen <stone@debian.org>  Tue, 23 Nov 2004 08:03:19 +0100

getmail4 (4.2.2-2) unstable; urgency=low

  * Fixed Build dep on python-dev Closes:#279512

 -- Fredrik Steen <stone@debian.org>  Wed, 27 Oct 2004 08:57:32 +0200

getmail4 (4.2.2-1) unstable; urgency=low

  * New upstream release

 -- Fredrik Steen <stone@debian.org>  Wed, 27 Oct 2004 08:52:23 +0200

getmail4 (4.2.0-1) unstable; urgency=low

  * Initial Release.

 -- Fredrik Steen <stone@debian.org>  Tue,  5 Oct 2004 08:44:52 +0200
